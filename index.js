var express = require("express");
var app = express();
var bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.post("/post", function (req, res) {
  return res.send("post world");
});
app.listen("3000", function () {
  console.log("Server listening on port 3000");
});
